<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use GuzzleHttp\Client;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            // get data from api (every hour)
            // check object if exists
            // create object if not exists

            // GuzzleHttp
            $client = new Client(); //GuzzleHttp\Client
            $res = $client->request('GET', 'http://176.119.33.251/temp');
            $array = json_decode($res->getBody(), true);

            if(is_numeric($array['temperature'])) {
                // Check object if exists
                $checkObject = \App\Weather::where('date', $array['date'])->exists();

                // Create object
                if($checkObject === false) {
                    $day = new \App\Weather();

                    $day->date = $array['date'];
                    $day->temperature = $array['temperature'];
                    $day->chance_for_rain = $array['chance_for_rain'];

                    $day->save();
                }
            }
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
