<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GetAvgTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:avg_temperature {days} {--queue=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get average temperature';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days');

        $subquery = \App\Weather::orderBy('date', 'desc')
            ->take($days)
            ->toSql();

        $average = number_format(DB::table(DB::raw(" ($subquery) as sub "))->avg('temperature'),1);

        $this->info('Average temperature is ' . $average);
    }
}
