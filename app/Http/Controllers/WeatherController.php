<?php

namespace App\Http\Controllers;

use App\Weather;
use Illuminate\Http\Request;

class WeatherController extends Controller
{

    public function index()
    {
        return view('weather');
    }

    public function store()
    {
        $data = Weather::orderBy('date', 'desc')
            ->take(7)
            ->get();

        return $data;
    }

    public function show(Request $request)
    {
        $data = \App\Weather::where('date', $request->date)->firstOrFail();
        return $data;
    }

    public function edit(Request $request)
    {
        $validatedData = $request->validate([
            'temperature' => 'required|integer'
        ]);

        $day = Weather::findOrFail($request->id);
        $day->temperature = $request->temperature;
        $day->save();
    }

}
