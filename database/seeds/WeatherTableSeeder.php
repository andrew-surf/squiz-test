<?php

use Illuminate\Database\Seeder;

class WeatherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate fake data for 10 days
        for ($i = 1; $i < 10; $i++) {

            $date = new DateTime('-' . $i . ' days');

            DB::table('weather')->insert([
                'date' => $date->format('Y-m-d'),
                'temperature' => rand(0, 30),
                'chance_for_rain' => rand(0, 100),
            ]);
        }
    }
}
