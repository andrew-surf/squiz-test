import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
Vue.use(require('vue-moment'));

import App from './views/App';
import Admin from './views/Admin';
import Home from './views/Home';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin,
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
});
