#!/bin/sh

if [ -n "$1" ]
then
    re='^[0-9]+$'
    if [[ $1 =~ $re ]]
    then
    # run command for laravel
    php artisan get:avg_temperature "$1"
    else
        echo "please input a number"
    fi
else
    echo "please specify parameter for this query"
fi
