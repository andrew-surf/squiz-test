## Wheater Forecast

PHP developer test

Project made with Laravel 

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

### Installing

- rename .env.example to .env
- change database name
- ```composer install```
- ```php artisan key:generate```
- ```php artisan migrate:fresh --seed```
- ```npm install``` & ```npm run dev```
- add the following Cron entry to your server:
```* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1```

#### Shell script
using shell script to print average temperature for last X days: ```bash weather.sh x```

#### Retrieving data from a web-server
cron task ```* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1```

This Cron will call the Laravel command scheduler every hour. Laravel will evaluate scheduled tasks and runs the tasks that are due.

My scheduled task is located here 
/app/Console/Kernel.php